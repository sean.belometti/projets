from .piece import *  # Importe toutes les classes des pièces


class Plateau:
    def __init__(self):
        self.plat = []
        self.create_plateau()

    ## Création de l'échiquier ##
    def draw_carres(self, fen):
        fen.fill(GREY)  # on remplit la fenêtre de gris
        for ran in range(RAN): # Dans chaque rangée...
            for col in range(ran % 2, COL, 2):  # En commençant soit à la colonne 0, soit à 1, en fonction de la rangée, tout les deux colonnes...
                pygame.draw.rect(fen, WHITE, (ran * SQUARE_SIZE, col * SQUARE_SIZE, SQUARE_SIZE, SQUARE_SIZE))  # On met un carré blanc

    ## Dessin d'une pièce à un nouvel emplacement ##
    def move(self, piece, ran, col):
        self.plat[piece.ran][piece.col], self.plat[ran][col] = self.plat[ran][col], self.plat[piece.ran][piece.col]
        if isinstance(piece, Paysan):
            if piece.color == BLACK and ran == 7:
                self.plat[piece.ran][piece.col] = 0
                self.plat[ran][col] = Reine(ran, col, BLACK)
            elif piece.color == WHITE and ran == 0:
                self.plat[piece.ran][piece.col] = 0
                self.plat[ran][col] = Reine(ran, col, WHITE)
        piece.move(ran, col)  # On donne à la pièce ses nouvelles coordonées, et elle se blit

    ## Retourne l'emplacement d'une pièce ##
    def quelle_piece(self, ran, col):
        return self.plat[ran][col]  # En fonction de la rangée et colonne fournient

    ## Création du plateau ##
    def create_plateau(self):
        for ran in range(RAN):
            self.plat.append([])  # Chaque rangée est sa propre liste
            for col in range(COL):
                if ran == 0:  # Première rangée
                    if col == 0 or col == 7:  # On met une tour noire de chaque coté
                        self.plat[ran].append(Tour(ran, col, BLACK))
                    elif col == 1 or col == 6:  # On met un cheval noir de chaque coté
                        self.plat[ran].append(Cheval(ran, col, BLACK))
                    elif col == 4:  # On met un roi noir
                        self.plat[ran].append(Roi(ran, col, BLACK))
                    elif col == 3:  # On met un reine noire
                        self.plat[ran].append(Reine(ran, col, BLACK))
                    elif col == 2 or col == 5:  # On met un fou noir de chaque coté
                        self.plat[ran].append(Fou(ran, col, BLACK))
                elif ran == 1:  # Deuxième rangée, on la rempli de paysans noirs
                    self.plat[ran].append(Paysan(ran, col, BLACK))
                elif ran == 6:  # Septième rangée, on la rempli de paysans blancs
                    self.plat[ran].append(Paysan(ran, col, WHITE))
                elif ran == 7:  # Huitième rangée
                    if col == 0 or col == 7:  # On met une tour blanche de chaque coté
                        self.plat[ran].append(Tour(ran, col, WHITE))
                    elif col == 1 or col == 6:  # On met un cheval blanch de chaque coté
                        self.plat[ran].append(Cheval(ran, col, WHITE))
                    elif col == 4:  # On met un roi blanc
                        self.plat[ran].append(Roi(ran, col, WHITE))
                    elif col == 3:  # On met un reine blanche
                        self.plat[ran].append(Reine(ran, col, WHITE))
                    elif col == 2 or col == 5:  # On met un fou blanc de chaque coté
                        self.plat[ran].append(Fou(ran, col, WHITE))
                else:  # Le reste est rempli de zéros
                    self.plat[ran].append(0)
        #print(self.plat)  # Pour voir à quoi ressemble le tableau, décommentez le print

    ## Dessin du plateau ##
    def draw(self, fen):
        self.draw_carres(fen)  # On dessine l'échiquier
        # Pour chaque rangée et colonne, tant qu'il n'y a pas un zéro, on dessine ce qu'il s'y trouve avec la méthode draw de la pièce en question
        for ran in range(RAN):
            for col in range(COL):
                piece = self.plat[ran][col]
                if piece != 0:
                    piece.draw(fen)

    ## Suppression de pièces ##
    def remove(self, current, maybe_mort, ran, col):
        for mort in maybe_mort:  # On regarde tout les morts possibles
            # Pour celui dont l'emplacement est le même que celui où notre pièce va se déplacer, on le supprime
            if (ran, col) == (mort.ran, mort.col):
                if isinstance(mort, Roi):
                    if current.color == BLACK:
                        print(' Les noirs gagnent !\n ESPACE pour réinitialiser.')
                    else:
                        print(' Les blancs gagnent !\n ESPACE pour réinitialiser.')
                self.plat[mort.ran][mort.col] = 0

    ###  Déplacements possibles avec un paysan ###
    def valid_depla_paysan(self, piece):
        ran, col = piece.ran, piece.col
        depla = []
        mort = []

        ###  Déplacements possibles avec un paysan blanc ###
        if piece.color == WHITE:
            ### Ligne pour les cases accesible en avançant veritcalement ###
            # Regarde en premier si la pièce peut avancer de deux (début)
            if ran == 6:
                # Doit donc voir si les deux rangées d'après et de la même colonne sont libres
                if self.plat[5][col] == 0 and self.plat[4][col] == 0:
                    # Si oui, ajoute l'élément à la liste
                    depla.append((4, col))
            # Vérifie que si l'on avance d'une rangée en ne dépassant pas la limite de l'échiquier et aussi que la case est libre
            if ran - 1 >= 0 and self.plat[ran - 1][col] == 0:
                # Si oui l'ajoute
                depla.append((ran - 1, col))
            ### Ligne pour manger des pions en diagonal (vu que notre pion est un paysan) ###
            # Déplacement en diagonal vers la gauche. Vérifie d'abord les valeurs limites, puis vérifie qu'il y a une pièce
            # Pour finir vérifie  que la couleur des deux pions soit différente
            if ran - 1 >= 0 and col - 1 >= 0 and self.plat[ran - 1][col - 1] != 0 and piece.color != self.plat[ran - 1][col - 1].color:
                # Si oui, l'ajoute
                depla.append((ran - 1, col - 1))
                mort.append(self.plat[ran - 1][col - 1])

            # Même chose que la condition d'en haut mais pour un déplacement en diagonal vers la droite
            if ran - 1 >= 0 and col + 1 <= 7 and self.plat[ran - 1][col + 1] != 0 and piece.color != self.plat[ran - 1][col + 1].color:
                depla.append((ran - 1, col + 1))
                mort.append(self.plat[ran - 1][col + 1])

        ###  Déplacement possible avec un paysan noir ###
        # Même principe que les lignes ci-dessus mais avec couleur noir (donc le pion descend)
        else:
            if ran == 1:
                if self.plat[2][col] == 0 and self.plat[3][col] == 0:
                    depla.append((3, col))
            if ran + 1 <= 7 and self.plat[ran + 1][col] == 0:
                depla.append((ran + 1, col))
            if ran + 1 <= 7 and col + 1 <= 7 and self.plat[ran + 1][col + 1] != 0 and piece.color != self.plat[ran + 1][col + 1].color:
                depla.append((ran + 1, col + 1))
                mort.append(self.plat[ran + 1][col + 1])
            if ran + 1 <= 7 and col - 1 >= 0 and self.plat[ran + 1][col - 1] != 0 and piece.color != self.plat[ran + 1][col - 1].color:
                depla.append((ran + 1, col - 1))
                mort.append(self.plat[ran + 1][col - 1])
        # Retourne la liste de toutes les cases où le pion peut aller
        return depla, mort


    ## Déplacements possibles pour le roi ##
    def valid_depla_roi(self, piece):
        ran, col = piece.ran, piece.col
        depla = []
        mort = []

        if ran - 1 >= 0:  # Peut-il aller au nord ?
            if self.plat[ran - 1][col] == 0:  # S'il n'y a rien, c'est un mouvement valide
                depla.append((ran - 1, col))
            elif piece.color != self.plat[ran - 1][col].color:  # S'il y a un ennemi, on peut l'éliminer
                depla.append((ran - 1, col))
                mort.append(self.plat[ran - 1][col])
            if col - 1 >= 0:  # Peut-il aller au nord-ouest ?
                if self.plat[ran - 1][col - 1] == 0:
                    depla.append((ran - 1, col - 1))
                elif piece.color != self.plat[ran - 1][col - 1].color:
                    depla.append((ran - 1, col - 1))
                    mort.append(self.plat[ran - 1][col - 1])
            if col + 1 <= 7:  # Peut-il aller au nord-est ?
                if self.plat[ran - 1][col + 1] == 0:
                    depla.append((ran - 1, col + 1))
                elif piece.color != self.plat[ran - 1][col + 1].color:
                    depla.append((ran - 1, col + 1))
                    mort.append(self.plat[ran - 1][col + 1])
        if ran + 1 <= 7:  # Peut-il aller au sud ?
            if self.plat[ran + 1][col] == 0:
                depla.append((ran + 1, col))
            elif piece.color != self.plat[ran + 1][col].color:
                depla.append((ran + 1, col))
                mort.append(self.plat[ran + 1][col])
            if col - 1 >= 0:  # Peut-il aller au sud-ouest ?
                if self.plat[ran + 1][col - 1] == 0:
                    depla.append((ran + 1, col - 1))
                elif piece.color != self.plat[ran + 1][col - 1].color:
                    depla.append((ran + 1, col - 1))
                    mort.append(self.plat[ran + 1][col - 1])
            if col + 1 <= 7:  # Peut-il aller au sud-est ?
                if self.plat[ran + 1][col + 1] == 0:
                    depla.append((ran + 1, col + 1))
                elif piece.color != self.plat[ran + 1][col + 1].color:
                    depla.append((ran + 1, col + 1))
                    mort.append(self.plat[ran + 1][col + 1])
        if col - 1 >= 0:  # Peut-il aller à l'ouest ?
            if self.plat[ran][col - 1] == 0:
                depla.append((ran, col - 1))
            elif piece.color != self.plat[ran][col - 1].color:
                depla.append((ran, col - 1))
                mort.append(self.plat[ran][col - 1])
        if col + 1 <= 7:  # Peut-il aller à l'est ?
            if self.plat[ran][col + 1] == 0:
                depla.append((ran, col + 1))
            elif piece.color != self.plat[ran][col + 1].color:
                depla.append((ran, col + 1))
                mort.append(self.plat[ran][col + 1])
        return depla, mort  # Retourne la liste de toutes les cases où le pion peut aller

    ## Déplacements possibles pour le cheval ##
    def valid_depla_cheval(self, piece):
        ran, col = piece.ran, piece.col
        depla = []
        mort = []

        if ran - 2 >= 0:  # Si le saut long au nord est possible...
            if col - 1 >= 0:  # Peut-il aller à l'ouest ?
                if self.plat[ran - 2][col - 1] == 0:  # S'il n'y a rien, c'est un mouvement valide
                    depla.append((ran - 2, col - 1))
                elif piece.color != self.plat[ran - 2][col - 1].color:  # S'il y a un ennemi, on peut l'éliminer
                    depla.append((ran - 2, col - 1))
                    mort.append(self.plat[ran - 2][col - 1])
            if col + 1 <= 7:  # Peut-il aller à l'est ?
                if self.plat[ran - 2][col + 1] == 0:
                    depla.append((ran - 2, col + 1))
                elif piece.color != self.plat[ran - 2][col + 1].color:
                    depla.append((ran - 2, col + 1))
                    mort.append(self.plat[ran - 2][col + 1])
        if ran + 2 <= 7:  # Si le saut long au sud est possible...
            if col - 1 >= 0:  # Peut-il aller à l'ouest ?
                if self.plat[ran + 2][col - 1] == 0:
                    depla.append((ran + 2, col - 1))
                elif piece.color != self.plat[ran + 2][col - 1].color:
                    depla.append((ran + 2, col - 1))
                    mort.append(self.plat[ran + 2][col - 1])
            if col + 1 <= 7:  # Peut-il aller à l'est ?
                if self.plat[ran + 2][col + 1] == 0:
                    depla.append((ran + 2, col + 1))
                elif piece.color != self.plat[ran + 2][col + 1].color:
                    depla.append((ran + 2, col + 1))
                    mort.append(self.plat[ran + 2][col + 1])
        if col - 2 >= 0:  # Si le saut long à l'ouest est possible...
            if ran - 1 >= 0:  # Peut-il aller au nord ?
                if self.plat[ran - 1][col - 2] == 0:
                    depla.append((ran - 1, col - 2))
                elif piece.color != self.plat[ran - 1][col - 2].color:
                    depla.append((ran - 1, col - 2))
                    mort.append(self.plat[ran - 1][col - 2])
            if ran + 1 <= 7:  # Peut-il aller au sud ?
                if self.plat[ran + 1][col - 2] == 0:
                    depla.append((ran + 1, col - 2))
                elif piece.color != self.plat[ran + 1][col - 2].color:
                    depla.append((ran + 1, col - 2))
                    mort.append(self.plat[ran + 1][col - 2])
        if col + 2 <= 7:  # Si le saut long à l'est est possible...
            if ran - 1 >= 0:  # Peut-il aller au nord ?
                if self.plat[ran - 1][col + 2] == 0:
                    depla.append((ran - 1, col + 2))
                elif piece.color != self.plat[ran - 1][col + 2].color:
                    depla.append((ran - 1, col + 2))
                    mort.append(self.plat[ran - 1][col + 2])
            if ran + 1 <= 7:  # Peut-il aller au sud ?
                if self.plat[ran + 1][col + 2] == 0:
                    depla.append((ran + 1, col + 2))
                elif piece.color != self.plat[ran + 1][col + 2].color:
                    depla.append((ran + 1, col + 2))
                    mort.append(self.plat[ran + 1][col + 2])
        return depla, mort  # Retourne la liste de toutes les cases où le pion peut aller
        
        
    ### Méthode pour trouver les mouvements verticaux d'une tour ou d'une reine ###
    def _veritcal_depla(self, piece):
        ran, col = piece.ran, piece.col
        rang = 1
        rang_haut = -1
        # Si la  pièce descend...
        while ran + rang <= 7:
            # Code pour trouver les cases disponibles pour une pièce allant en bas
            if self.plat[ran + rang][col] == 0:  # S'il n'y a rien en bas ...
                self.depla.append((ran + rang, col))  # On ajoute le déplacement dans la liste
            elif self.plat[ran + rang][col].color != piece.color:  # S'il y a un ennemi...
                self.depla.append((ran + rang, col))  # On ajoute le déplacement dans la liste
                self.mort.append(self.plat[ran + rang][col])  # On ajoute l'énnemi comme victime potentielle
                break
            else:  # Sinon, un allié est dans le chemin
                break  # On arrête de chercher d'autres déplacement possibles en bas
            rang += 1  # A chaque passage, nous devons augmenter cette variable pour vérifier les cases d'après

        # Si la pièce monte...
        # Meme principe que les lignes ci-dessus sauf que la variable rang_haut diminue pour monter
        while ran + rang_haut >= 0:
            if self.plat[ran + rang_haut][col] == 0:
                self.depla.append((ran + rang_haut, col))
            elif self.plat[ran + rang_haut][col].color != piece.color:
                self.depla.append((ran + rang_haut, col))
                self.mort.append(self.plat[ran + rang_haut][col])
                break
            else:
                break
            rang_haut += -1    
            
    ### Méthode pour trouver les mouvements horizontaux d'une tour our d'une reine ###
    def _horizontal_depla(self, piece):
        ran, col = piece.ran, piece.col
        horizontal_droite = 1
        horizontal_gauche = -1
        ### Code pour trouver les déplacements horizontaux vers la droite###
        while col + horizontal_droite <= 7:  # pour ne pas dépacer l'échiquier
            # Si la case d'après est libre, l'ajoute aux mouvements valables
            if self.plat[ran][col + horizontal_droite] == 0:
                self.depla.append((ran, col + horizontal_droite))
            # Sinon, vérifie la couleur
            elif self.plat[ran][col + horizontal_droite].color != piece.color:
                # Si la couleur est différente, peut y aller mais doit ensuite
                # sortir de la boucle
                self.depla.append((ran, col + horizontal_droite))
                self.mort.append(self.plat[ran][col + horizontal_droite]) # On ajoute l'énnemi comme victime potentielle
                break
            # Si aucune condition n'est remplie, la pièce ne peut pas aller vers la droite
            # Sort donc de la boucle
            else:
                break
            # Incrémente à chaque passage la variable horizontal_droite
            horizontal_droite += 1
        ### Code pour trouver les déplacements horizontaux de la pièce vers la gauche
        # Même principe que celui du dépacement vers la droite, sauf que cette fois-ci,
        # Variable horizontal_gauche diminue
        while col + horizontal_gauche >= 0:
            if self.plat[ran][col + horizontal_gauche] == 0:
                self.depla.append((ran, col + horizontal_gauche))
            elif self.plat[ran][col + horizontal_gauche].color != piece.color:
                self.depla.append((ran, col + horizontal_gauche))
                self.mort.append(self.plat[ran][col + horizontal_gauche])
                break
            else:
                break
            horizontal_gauche += -1
            
    ### déplacement en diagonal vers le bas d'une pièce ###
    def _diagaonal_bas(self, piece):
        ran, col = piece.ran, piece.col
        rang = 1
        diago_droite = 1
        diago_gauche = -1

        # Si le pion descend...
        while ran + rang <= 7 and col + diago_droite <=7: # Pour ne pas dépasser l'échiquier
            # Code pour trouver les cases disponibles pour un pion allant dans une diagonal en bas vers la droite
            if self.plat[ran + rang][col + diago_droite] == 0:  # S'il n'y a rien en bas à droite du pion...
                self.depla.append((ran + rang, col + diago_droite))  # On ajoute le déplacement dans la liste
            elif self.plat[ran + rang][col + diago_droite].color != piece.color:  # S'il y a un ennemi...
                self.depla.append((ran + rang, col + diago_droite))  # On ajoute le déplacement dans la liste
                self.mort.append(self.plat[ran + rang][col + diago_droite])  # On ajoute l'énnemi comme victime potentielle
                break # On arrête de chercher d'autres déplacement possibles en bas à droite
            else:  # Sinon, un allié est dans le chemin
                break # On arrête de chercher d'autres déplacement possibles en bas à droite
            # A chaque passage, nous devons augmenter ces variables pour vérifier les cases d'après
            diago_droite += 1
            rang +=1
                
        # Code pour trouver les cases disponibles pour une pièce allant dans une diagonal en bas vers la gauche
        # Même chose qu'en haut
        rang = 1
        while ran + rang <= 7 and col + diago_gauche >= 0:
            if self.plat[ran + rang][col + diago_gauche] == 0:
                self.depla.append((ran + rang, col + diago_gauche))
            elif self.plat[ran + rang][col + diago_gauche].color != piece.color:
                self.depla.append((ran + rang, col + diago_gauche))
                self.mort.append(self.plat[ran + rang][col + diago_gauche])
                break
            else:
                break
            
            rang += 1
            diago_gauche += -1
            
     ### Méthode pour trouver les mouvements en diagonal vers le haut ###
    def _diagaonal_haut(self, piece):
        # Si la pièce monte...
        # Meme principe que les lignes ci-dessus sauf que la variable rang diminue pour monter
        ran, col = piece.ran, piece.col
        rang = -1
        diago_droite = 1
        diago_gauche = -1
        while ran + rang >= 0 and col + diago_droite <=7:
            if self.plat[ran + rang][col + diago_droite] == 0:
                self.depla.append((ran + rang, col + diago_droite))
            elif self.plat[ran + rang][col + diago_droite].color != piece.color:
                self.depla.append((ran + rang, col + diago_droite))
                self.mort.append(self.plat[ran + rang][col + diago_droite])
                break
            else:
                break
            rang += -1
            diago_droite += 1
            
        rang = -1
        while ran + rang >= 0 and col + diago_gauche >= 0:
            if self.plat[ran + rang][col + diago_gauche] == 0:
                self.depla.append((ran + rang, col + diago_gauche))
            elif self.plat[ran + rang][col + diago_gauche].color != piece.color:
                self.depla.append((ran + rang, col + diago_gauche))
                self.mort.append(self.plat[ran + rang][col + diago_gauche])
                break
            else:
                break
            rang += -1
            diago_gauche += -1
    
    
    
    
    def valid_depla_tour(self, piece):
        # Vide ces deux variables
        self.depla = []# Liste des élément où la pièce peut se déplacer
        self.mort = []  # Liste des victimes potentielles
        # la tour se déplace veritcalement et horizontalement
        self._veritcal_depla(piece)
        self._horizontal_depla(piece)
        return self.depla, self.mort  # On return la liste des déplacements possibles, des victimes potentielles et la pièce qui va bouger



    def valid_depla_fou(self, piece):
        # Vide ces deux variables
        self.depla = []# Liste des élément où la pièce peut se déplacer
        self.mort = []  # Liste des victimes potentielles
        # Le fou se déplace en diagonal
        self._diagaonal_bas(piece)
        self._diagaonal_haut(piece)
        return self.depla, self.mort  # On return la liste des déplacements possibles, des victimes potentielles et la pièce qui va bouger
        
    def valid_depla_reine(self, piece):
        # Vide ces deux variables
        self.depla = []  # Liste des élément où la pièce peut se déplacer
        self.mort = []  # Liste des victimes potentielles
        # Reine se déplace verticalement, hoeizontalement et en diablonal
        self._diagaonal_bas(piece)
        self._diagaonal_haut(piece)
        self._veritcal_depla(piece)
        self._horizontal_depla(piece)
        return self.depla, self.mort  # On return la liste des déplacements possibles, des victimes potentielles et la pièce qui va bouger
        
        
        
