//alert('aaaaaaaaa')

//playing field
const cvs = document.getElementById('play');
const ctx = cvs.getContext('2d');

//load images
let woodBgBol = new Image();
woodBgBol.src = "images/woodfloubol.jpg";

let woodBg = new Image();
woodBg.src = "images/woodflou.jpg";

let playButton = new Image();
playButton.src = "images/playButton.jpg";

let bolEmpty = new Image();
bolEmpty.src = "images/bolEmpty.png";

let bolCarre = new Image();
bolCarre.src = "images/bolCarre.png";

let bolTiangle = new Image();
bolTiangle.src = "images/bolTiangle.png";

let bolCircle = new Image();
bolCircle.src = "images/bolCircle.png";

let bolLosange = new Image();
bolLosange.src = "images/bolLosange.png";

let carreImg = new Image();
carreImg.src = "images/carre.png";

let tiangleImg = new Image();
tiangleImg.src = "images/tiangle.png";

let circleImg = new Image();
circleImg.src = "images/circle.png";

let losangeImg = new Image();
losangeImg.src = "images/losange.png";

//random shape
const possibleShape = [carreImg, tiangleImg, circleImg, losangeImg];
let currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];

//create shape coordinates variable
const possibleX = [57, 204, 351, 498];
let shape = {
	x : possibleX[Math.floor(Math.random()*possibleX.length)],
	y : 20
}

//random bowl x coordinate
const possibleBolX = [17, 164, 311, 458];

let bolCarreX = possibleBolX[Math.floor(Math.random()*possibleBolX.length)];
possibleBolX.splice (possibleBolX.indexOf(bolCarreX), 1);

let bolTiangleX = possibleBolX[Math.floor(Math.random()*possibleBolX.length)];
possibleBolX.splice (possibleBolX.indexOf(bolTiangleX), 1);

let bolCircleX = possibleBolX[Math.floor(Math.random()*possibleBolX.length)];
possibleBolX.splice (possibleBolX.indexOf(bolCircleX), 1);

let bolLosangeX = possibleBolX[Math.floor(Math.random()*possibleBolX.length)];

//create score variable
let score = 0;

//create error counter
let wrong = 0;

//create shape speed variable
let fallSpeed = 1;

//shape count variable
let shapeCount = 0;

//call time variable
let callTime = 90;

//high score variable
let highScore = 0;

//win variable
let winCond = 20;

//control the shapes
document.addEventListener('keydown',direction);
function direction(event) {
	if (event.keyCode == 37 && shape.x !== 57) {
		shape.x = shape.x - 147;
	}
	else if (event.keyCode == 39 && shape.x !== 498) {
		shape.x = shape.x + 147;
	}
	
}

//falling shape
function descendshape() {
	if (shape.y < 500) {
		shape.y = shape.y + fallSpeed;
	}
	else {
		if (currentShape == carreImg && shape.x == bolCarreX + 40) {
			score++;
			shapeCount++;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
		else if (currentShape == tiangleImg && shape.x == bolTiangleX + 40) {
			score++;
			shapeCount++;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
		else if (currentShape == circleImg && shape.x == bolCircleX + 40) {
			score++;
			shapeCount++;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
		else if (currentShape == losangeImg && shape.x == bolLosangeX + 40) {
			score++;
			shapeCount++;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
		else {
			wrong++;
			shapeCount++;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
	}
}

//draw title screen on canvas
function drawtitle() {
	ctx.drawImage(woodBg,0,0,608,608);
	/*if () {
		game = setInterval(drawgame,90);
	}*/
}

//draw game on canvas
function drawgame() {
	ctx.drawImage(woodBgBol,0,0,608,608);
	ctx.drawImage(currentShape,shape.x,shape.y,50,50);
	ctx.drawImage(bolCarre,bolCarreX,488,130,66);
	ctx.drawImage(bolTiangle,bolTiangleX,488,130,66);
	ctx.drawImage(bolCircle,bolCircleX,488,130,66);
	ctx.drawImage(bolLosange,bolLosangeX,488,130,66);
	
	ctx.fillStyle = 'green';
	ctx.font = "30px Changa one";
	ctx.fillText(score + '/20',20,40);
	
	ctx.fillStyle = 'red';
	ctx.font = "30px Changa one";
	ctx.fillText(wrong + '/3',548,40);
	
	ctx.fillStyle = 'green';
	ctx.font = "30px Changa one";
	ctx.fillText(highScore,20,80);
	
	//speed up
	if (shapeCount == 5) {
		fallSpeed++;
		shapeCount = 0;
	}
	
	//win condition
	if (score == winCond) {
		alert('GAGNÉ !' + 
			  "\nESSAIE D'ALLER LE PLUS LOIN POSSIBLE !");
		winCond = 100000000000000;
	}
	
	//game over
	if (wrong == 3) {
		if (score > highScore) {
			highScore = score;
			score = 0;
			fallSpeed = 1;
			shapeCount = 0;
			alert('PERDU !' + 
				  '\nTON RECORD A ÉTÉ MIS À JOUR');
			wrong = 0;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
		else {
			score = 0;
			fallSpeed = 1;
			shapeCount = 0;
			alert('PERDU !' + 
				  '\nRÉESSAIE !');
			wrong = 0;
			currentShape = possibleShape[Math.floor(Math.random()*possibleShape.length)];
			shape = {
				x : possibleX[Math.floor(Math.random()*possibleX.length)],
				y : 20
			}
		}
	}
}

//framerate
let game = setInterval(drawgame,callTime);
let dshape = setInterval(descendshape,10);