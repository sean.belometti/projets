from .plateau import Plateau  # Importe la classe Plateau
from .piece import *  # Importe toutes les classes des pièces


class Jeu:
    def __init__(self, fen):
        self._init()
        self.fen = fen

    # Méthode pour rafraîchir le jeu
    def update(self):
        self.plateau.draw(self.fen)
        self.draw_moves(self.valid_moves)
        pygame.display.update()

    # Le init est séparé pour qu'on puisse reset facilement
    def _init(self):
        self.selected = None
        self.plateau = Plateau()
        self.tour = WHITE
        self.valid_moves = []
        self.to_remove = []
        self.current = None

    # Méthode pour reset
    def reset(self):
        self._init()

    # Méthode qui détermine sur quoi on a cliqué
    def select(self, ran, col):
        if self.selected:  # Si il y a déjà une pièce sélectionnée, on essaye de la bouger
            result = self._move(ran, col)  # Retourne True ou False, en fonction de si on a cliqué sur un déplacement valid ou non
            if not result:  # Si on n'a pas cliqué sur un déplacement valide, on reset les valides moves et la sélection
                self.valid_moves = []
                self.selected = None
                self.select(ran, col)
        piece = self.plateau.quelle_piece(ran, col)  # Si on veut sélectionner une pièce, on la trouve avec la méthode de la classe Plateau
        if piece != 0:  # On s'assure qu'on a bien une pièce...
            # C'est la même chose pour toutes les pièces
            if isinstance(piece, Paysan) and piece.color == self.tour:  # Si c'est un paysan et que c'est son tour de bouger...
                self.selected = piece  # On indique qu'elle est sélectionnée
                # On tire les déplacements possibles et les morts potentiels
                self.valid_moves, self.to_remove = self.plateau.valid_depla_paysan(piece)
                return True
            if isinstance(piece, Tour) and piece.color == self.tour:
                self.selected = piece
                self.valid_moves, self.to_remove = self.plateau.valid_depla_tour(piece)
                return True
            elif isinstance(piece, Roi) and piece.color == self.tour:
                self.selected = piece
                self.valid_moves, self.to_remove = self.plateau.valid_depla_roi(piece)
                return True
            elif isinstance(piece, Cheval) and piece.color == self.tour:
                self.selected = piece
                self.valid_moves, self.to_remove = self.plateau.valid_depla_cheval(piece)
                return True
            elif isinstance(piece, Reine) and piece.color == self.tour:
                self.selected = piece
                self.valid_moves, self.to_remove = self.plateau.valid_depla_reine(piece)
                return True
            elif isinstance(piece, Fou) and piece.color == self.tour:
                self.selected = piece
                self.valid_moves, self.to_remove = self.plateau.valid_depla_fou(piece)
                return True

        return False

    # Méthode qui regarde si on peut bouger là où on a cliqué
    def _move(self, ran, col):
        if self.selected and (ran, col) in self.valid_moves:  # Si on a cliqué sur un déplacement valide...
            if self.to_remove:  # Si il y a des morts potentiels...
                self.plateau.remove(self.selected, self.to_remove, ran, col)  # On lance la méthode remove de la classe Plateau
            self.plateau.move(self.selected, ran, col)  # Après avoir supprimé ce qu'il faut, on bouge la pièce
            self.change_turn()  # On change de tour
        else:
            return False

        return True

    # Méthode qui dessine les déplacements possibles à l'écran
    def draw_moves(self, moves):
        for move in moves:
            ran, col = move
            pygame.draw.circle(self.fen, GREEN, (col * SQUARE_SIZE + SQUARE_SIZE//2, ran * SQUARE_SIZE + SQUARE_SIZE//2), SQUARE_SIZE // 15)

    # Méthode qui change le tour
    def change_turn(self):
        self.valid_moves = []
        self.to_remove = []
        if self.tour == BLACK:
            self.tour = WHITE
        else:
            self.tour = BLACK
