import pygame

WIDTH, HEIGHT = 800, 800  # Toutes les autres valeurs de tailles dépendent de WIDTH et HEIGHT. En les modifiant, le reste change avec. Essayez!
RAN, COL = 8, 8
SQUARE_SIZE = WIDTH//COL

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREY = (75, 75, 75)
GREEN = (10, 200, 30)

IMAGE_W = SQUARE_SIZE // 2
IMAGE_H = SQUARE_SIZE // 2

PAYSAN_N = pygame.transform.scale(pygame.image.load('images/paysan_noir.png'), (IMAGE_W, IMAGE_H))
PAYSAN_B = pygame.transform.scale(pygame.image.load('images/paysan_blanc.png'), (IMAGE_W, IMAGE_H))
TOUR_N = pygame.transform.scale(pygame.image.load('images/tour_noir.png'), (IMAGE_W, IMAGE_H))
TOUR_B = pygame.transform.scale(pygame.image.load('images/tour_blanc.png'), (IMAGE_W, IMAGE_H))
REINE_N = pygame.transform.scale(pygame.image.load('images/reine_noir.png'), (IMAGE_W, IMAGE_H))
REINE_B = pygame.transform.scale(pygame.image.load('images/reine_blanc.png'), (IMAGE_W, IMAGE_H))
ROI_N = pygame.transform.scale(pygame.image.load('images/roi_noir.png'), (IMAGE_W, IMAGE_H))
ROI_B = pygame.transform.scale(pygame.image.load('images/roi_blanc.png'), (IMAGE_W, IMAGE_H))
CHEVAL_N = pygame.transform.scale(pygame.image.load('images/cheval_noir.png'), (IMAGE_W, IMAGE_H))
CHEVAL_B = pygame.transform.scale(pygame.image.load('images/cheval_blanc.png'), (IMAGE_W, IMAGE_H))
FOU_N = pygame.transform.scale(pygame.image.load('images/fou_noir.png'), (IMAGE_W, IMAGE_H))
FOU_B = pygame.transform.scale(pygame.image.load('images/fou_blanc.png'), (IMAGE_W, IMAGE_H))