from .constantes import *  # Importe les constantes


## Toutes les classes de pièce sont faites de la même façon...


class Paysan:
    def __init__(self, ran, col, color):
        self.ran = ran
        self.col = col
        self.x = self.col * SQUARE_SIZE + IMAGE_W // 2  # On place la pièce de façon à ce qu'elle apparaisse au centre de la case
        self.y = self.ran * SQUARE_SIZE + IMAGE_H // 2
        self.color = color

    # Blit la bonne couleur de la pièce
    def draw(self, fen):
        if self.color == BLACK:
            fen.blit(PAYSAN_N, (self.x, self.y))
        else:
            fen.blit(PAYSAN_B, (self.x, self.y))

    # Rafraîchit la rangée et colonne si movement de la pièce il y a
    def move(self, ran, col):
        self.ran = ran
        self.col = col
        self.calc_pos()

    # Calcul de ses nouvelles coordonées pour la blit
    def calc_pos(self):
        self.x = SQUARE_SIZE * self.col + IMAGE_W // 2
        self.y = SQUARE_SIZE * self.ran + IMAGE_H // 2

    # Meilleure représentation de l'objet pour le débogage
    def __repr__(self):
        if self.color == BLACK:
            return str('PAYSAN_NOIR')
        else:
            return str('PAYSAN_BLANC')


class Tour:
    def __init__(self, ran, col, color):
        self.ran = ran
        self.col = col
        self.x = self.col * SQUARE_SIZE + IMAGE_W // 2
        self.y = self.ran * SQUARE_SIZE + IMAGE_H // 2
        self.color = color

    def draw(self, fen):
        if self.color == BLACK:
            fen.blit(TOUR_N, (self.x, self.y))
        else:
            fen.blit(TOUR_B, (self.x, self.y))
    
    def move(self, ran, col):
        self.ran = ran
        self.col = col
        self.calc_pos()
    
    def calc_pos(self):
        self.x = SQUARE_SIZE * self.col + IMAGE_W // 2
        self.y = SQUARE_SIZE * self.ran + IMAGE_H // 2
        
    def __repr__(self):
        if self.color == BLACK:
            return str('TOUR_NOIR')
        else:
            return str('TOUR_BLANC')


class Reine:
    def __init__(self, ran, col, color):
        self.ran = ran
        self.col = col
        self.x = self.col * SQUARE_SIZE + IMAGE_W // 2
        self.y = self.ran * SQUARE_SIZE + IMAGE_H // 2
        self.color = color

    def draw(self, fen):
        if self.color == BLACK:
            fen.blit(REINE_N, (self.x, self.y))
        else:
            fen.blit(REINE_B, (self.x, self.y))
    
    def move(self, ran, col):
        self.ran = ran
        self.col = col
        self.calc_pos()
    
    def calc_pos(self):
        self.x = SQUARE_SIZE * self.col + IMAGE_W // 2
        self.y = SQUARE_SIZE * self.ran + IMAGE_H // 2
        
    def __repr__(self):
        if self.color == BLACK:
            return str('REINE_NOIR')
        else:
            return str('REINE_BLANC')


class Roi:
    def __init__(self, ran, col, color):
        self.ran = ran
        self.col = col
        self.x = self.col * SQUARE_SIZE + IMAGE_W // 2
        self.y = self.ran * SQUARE_SIZE + IMAGE_H // 2
        self.color = color

    def draw(self, fen):
        if self.color == BLACK:
            fen.blit(ROI_N, (self.x, self.y))
        else:
            fen.blit(ROI_B, (self.x, self.y))
            
    def move(self, ran, col):
        self.ran = ran
        self.col = col
        self.calc_pos()
    
    def calc_pos(self):
        self.x = SQUARE_SIZE * self.col + IMAGE_W // 2
        self.y = SQUARE_SIZE * self.ran + IMAGE_H // 2
        
    def __repr__(self):
        if self.color == BLACK:
            return str('ROI_NOIR')
        else:
            return str('ROI_BLANC')


class Cheval:
    def __init__(self, ran, col, color):
        self.ran = ran
        self.col = col
        self.x = self.col * SQUARE_SIZE + IMAGE_W // 2
        self.y = self.ran * SQUARE_SIZE + IMAGE_H // 2
        self.color = color

    def draw(self, fen):
        if self.color == BLACK:
            fen.blit(CHEVAL_N, (self.x, self.y))
        else:
            fen.blit(CHEVAL_B, (self.x, self.y))

    def move(self, ran, col):
        self.ran = ran
        self.col = col
        self.calc_pos()

    def calc_pos(self):
        self.x = SQUARE_SIZE * self.col + IMAGE_W // 2
        self.y = SQUARE_SIZE * self.ran + IMAGE_H // 2

    def __repr__(self):
        if self.color == BLACK:
            return str('CHEVAL_NOIR')
        else:
            return str('CHEVAL_BLANC')

class Fou:
    def __init__(self, ran, col, color):
        self.ran = ran
        self.col = col
        self.x = self.col * SQUARE_SIZE + IMAGE_W // 2
        self.y = self.ran * SQUARE_SIZE + IMAGE_H // 2
        self.color = color

    def draw(self, fen):
        if self.color == BLACK:
            fen.blit(FOU_N, (self.x, self.y))
        else:
            fen.blit(FOU_B, (self.x, self.y))

    def move(self, ran, col):
        self.ran = ran
        self.col = col
        self.calc_pos()

    def calc_pos(self):
        self.x = SQUARE_SIZE * self.col + IMAGE_W // 2
        self.y = SQUARE_SIZE * self.ran + IMAGE_H // 2

    def __repr__(self):
        if self.color == BLACK:
            return str('FOU_NOIR')
        else:
            return str('FOU_BLANC')
