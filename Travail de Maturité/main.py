#!/usr/bin/env python

from gimpfu import *
import cv2

def detect(uri):
    i = 0
    faceCascade = cv2.CascadeClassifier('/home/pi/.config/GIMP/2.10/plug-ins/haarcascade_frontalface_default.xml') # Remplacez 'pi' par votre nom d'utilisateur dans l'emplacement
    imgcv = cv2.imread(uri)
    imgGray = cv2.cvtColor(imgcv, cv2.COLOR_BGR2GRAY)
    
    faces = faceCascade.detectMultiScale(imgGray, 1.3, 2)
    
    for (x, y, w, h) in faces:
        detected_face = imgcv[int(y):int(y+h), int(x):int(x+w)]
        detected_face_blurred = blur_img(detected_face, factor = 3)
        imgcv[y:y+h, x:x+w] = detected_face_blurred
        i += 1
    
    status = cv2.imwrite('/home/pi/img.png', imgcv) # Modifiez ici l'emplacement de sauvegarde de l'image
    number = str(i)
    pdb.gimp_message(number + ' faces detected!')
    
    return faces


def blur_img(imgcv, factor = 20):
    kW = int(imgcv.shape[1] / factor)
    kH = int(imgcv.shape[0] / factor)
    
    if kW % 2 == 0: kW += -1
    if kH % 2 == 0: kH += -1
    
    blurred_img = cv2.GaussianBlur(imgcv, (kW,kH), 0)
    return blurred_img
    

def image_sender():
    ima = gimp.image_list()[0]
    uri = pdb.gimp_image_get_imported_uri(ima)
    #pdb.gimp_message(uri)
    if uri.startswith('file://'):
        uri = uri[7:]
    #pdb.gimp_message(uri)
    if detect(uri) != ():
        calque = pdb.gimp_file_load_layer(ima,'/home/pi/img.png')
        ima.add_layer(calque, -1)
    else:
        pdb.gimp_message('No faces detected!')

register(
    "python-fu-face-detection",
    "Detect faces",
    "Detect the faces in the image",
    "Sean Belometti", "Sean Belometti", "2020",
    "Foggy Face",
    "",
    [],
    [],
    image_sender, menu="<Image>/Filters/Blur")

main()
