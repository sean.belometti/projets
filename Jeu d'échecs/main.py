import pygame
from echecs.constantes import WIDTH, HEIGHT, SQUARE_SIZE  # Inporte les valeurs définies dans le fichier constantes.py
from echecs.jeu import Jeu  # Importe la classe Jeu

FPS = 60  # Définit le nombre d'images par seconde

FEN = pygame.display.set_mode((WIDTH, HEIGHT))  # Crée la fenêtre avec les constantes adéquates
pygame.display.set_caption('JEU ECHECS')  # Lui donne un nom


# Fonction qui retourne la rangée et la colonne sur lesquelles se trouve la souris
def get_ran_col_from_mouse(pos):
    x, y = pos
    ran = y // SQUARE_SIZE
    col = x // SQUARE_SIZE
    return ran, col


# Boucle pygame
def main():
    run = True
    clock = pygame.time.Clock()
    jeu = Jeu(FEN)  # Initialise l'objet jeu

    while run:
        clock.tick(FPS)

        for event in pygame.event.get():  # On prend tout les évènements
            if event.type == pygame.QUIT: # Si on presse sur la croix, on quitte
                run = False

            # Après un clic de la souris...
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()  # On prend ses coordonnées
                ran, col = get_ran_col_from_mouse(pos)  # la rengée, colonne
                jeu.select(ran, col)  # On lance la méthode qui s'occupe de la sélection

            # Reset le jeu en pressant sur espace
            keys = pygame.key.get_pressed()
            if keys[pygame.K_SPACE]:
                jeu.reset()

        jeu.update()  # Rafraîchit

    pygame.quit()


main()
